import { defineConfig } from "cypress";

export default defineConfig({
  // Set the viewport dimensions
  viewportWidth: 1920,

  viewportHeight: 1080,

  // Disable web security for cross-origin framing
  // Be careful with this option, it can have serious security implications
  chromeWebSecurity: false,

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
