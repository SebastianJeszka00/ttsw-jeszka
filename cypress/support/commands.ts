Cypress.Commands.add("login", () => {
  const credentials = require("../fixtures/example.json");
  cy.visit(credentials.url);
  cy.get('input[id="login"]').type(credentials.username);
  cy.get('input[id="password"]').type(credentials.password);
  cy.get("button").contains("LOGIN").click();
});

Cypress.Commands.add("PermissionGroupList", () => {
  cy.contains('.layout-menuitem-text', 'Permission groups list').click({
    force: true
  });
});

Cypress.Commands.add("PermissionForTabItems", () => {
  cy.contains('.layout-menuitem-text', 'Permissions for tab items').click({
    force: true
  });
});

Cypress.Commands.add("SourceList", () => {
  cy.contains('.layout-menuitem-text', 'Sources list').click({
    force: true
  });
});

Cypress.Commands.add("MeasuringSourceList", () => {
  cy.contains('.layout-menuitem-text', 'Measuring points list').click({
    force: true
  });
});

Cypress.Commands.add("darkMode", () => {
cy.get(".dark-mode").click();
});

Cypress.Commands.add("changeLanguage", () => {
  cy.get('.change_language').click()
});
Cypress.Commands.add("logOut", () => {
  cy.get('.logout-link').click()
});

//cypress.env
