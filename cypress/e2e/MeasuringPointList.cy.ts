import MeasuringPointListPage from './MeasuringPointListPage';

describe("Measuring source list", () => {
    const selectDropdownOption = (dropdownSelector: string, option: string) => {
        cy.get(dropdownSelector).click();
        cy.contains(option).click();
    };

    const addNewList = (
        idCode: string, 
        nameValue: string, 
        productType: string, 
        operator: string, 
        group: string, 
        desc: string
    ) => {
        // Adding a new form
        cy.get('[icon="pi pi-plus"] > .p-ripple > .p-button-label').click();
        cy.get('#p-accordiontab-0');
        cy.get('#p-accordiontab-0');

        // Filling the form
        cy.get('#identifierCode').type(idCode);
        cy.get('#name').type(nameValue);

        selectDropdownOption('#productType > .p-dropdown > .p-dropdown-label', productType); 
        selectDropdownOption('#operator > .p-dropdown > .p-dropdown-label', operator);
        selectDropdownOption('#group > .p-dropdown > .p-dropdown-label', group);

        cy.get('#dateFrom > .p-calendar > .p-inputtext').type('2023-10-27');
        cy.get('#dateTo > .p-calendar > .p-inputtext').type('2023-10-27');

        cy.get('#description').type(desc);
    };

    before(() => {
        cy.login();
        cy.MeasuringSourceList();
    });

    it("should perform all operations after login", () => {
        // Add a new item
        addNewList('002023', 'Tester Testowy', 'ENERGY', 'test', 'Test123', 'lorem');
        cy.contains('button', 'Save').click();
        cy.contains('button', 'Close').click();
        cy.get('#rejectCloseEditDialog').click();
        cy.get('.p-dialog-header-minus-icon').click();
    });
});
