import PermissionGroupListPage from './PermissionGroupListPage';

describe("Test Source List", () => {
    before(() => {
        cy.login();
        cy.PermissionGroupList();
    });

    const addNewButton = '#addNew > .p-ripple > .p-button-label';
    const cellEditorInput = 'p-celleditor.p-element > .p-inputtext';
    const saveButton = '.p-button-success';
    const editButton = ':nth-child(27) > :nth-child(3) > .flex > .p-button-primary';
    const deleteButton = ':nth-child(27) > :nth-child(3) > .flex > p-button.p-element > .p-ripple';

    it("should perform all operations after login", () => {
        // Add a new item
        cy.wait(1000)
        .get(addNewButton)
        .should('be.visible')  
        .click();

        cy.get(cellEditorInput)
        .type('testowy element 2');

        cy.get('.p-button-success').click();

        // Edit an item
        cy.get(editButton).click();
        cy.get(cellEditorInput)
        .type('{selectall} zedytowany testowy element');
        cy.get(saveButton).click();

        // Delete an item
        cy.get(deleteButton).click();
        cy.get('.p-dialog-header-close-icon').click();
        cy.get(deleteButton).click();
        cy.get('.p-button-outlined').click();
        cy.get(deleteButton).click();
        cy.get('[ng-reflect-label="Delete"]').click();

        // Change role
        const roleDropdownSelector = '#pr_id_4_label';
        const userRoleSelector = '[ng-reflect-label="User"] > .p-ripple';
        const superUserRoleSelector = '[ng-reflect-label="SuperUserSuperUserSuperUserSup"] > .p-ripple';
        cy.get(roleDropdownSelector).click();
        cy.get(userRoleSelector).click();
        cy.wait(40);
        cy.get(roleDropdownSelector).click();
        cy.get(superUserRoleSelector).click();
    });
});
