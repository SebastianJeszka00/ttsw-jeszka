
import DarkModePage from './DarkModePage';
describe("SomeTest", () => {
  beforeEach(() => {
    cy.login();
  });

  it("should switch to dark mode", () => {
    cy.darkMode();
  });
});
