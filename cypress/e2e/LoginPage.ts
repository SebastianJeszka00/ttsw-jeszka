
class LoginPage {
  private readonly url = require("../fixtures/example.json").url;
  private readonly usernameInput = 'input[id="login"]';
  private readonly passwordInput = 'input[id="password"]';
  private readonly loginButton = 'button:contains("LOGIN")';

  public visit() {
    cy.visit(this.url);
  }

  public typeUsername(username: string) {
    cy.get(this.usernameInput).type(username);
  }

  public typePassword(password: string) {
    cy.get(this.passwordInput).type(password);
  }

  public clickLoginButton() {
    cy.get(this.loginButton).click();
  }

  public verifyLoginSuccess(expectedUrl: string) {
    cy.url().should("eq", expectedUrl);
  }
}

export default new LoginPage();
