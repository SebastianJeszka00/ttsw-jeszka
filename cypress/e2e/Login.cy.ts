
import LoginPage from './LoginPage';
const credentials = require("../fixtures/example.json");

describe("LoginPage", () => {
  it("should log in with valid credentials", () => {
    // Open the login page using the URL from the JSON file
    LoginPage.visit();

    LoginPage.typeUsername(credentials.username);
    LoginPage.typePassword(credentials.password);

    // Click the login button
    LoginPage.clickLoginButton();

    // Ensure that the login was successful
    LoginPage.verifyLoginSuccess("http://luxmdm-itil.ttsw.com.pl/");
  });
});
