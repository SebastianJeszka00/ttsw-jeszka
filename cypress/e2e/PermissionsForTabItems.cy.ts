import PermissionsForTabItemsPage from './PermissionsForTabItemsPage';

describe("Permission For Tab Items Test", () => {
    before(() => {
        cy.login();
        cy.PermissionForTabItems();
    });

    it("should perform all operations after login", () => {
        // Select options
        const rows = [1, 2];
        const columns = [2, 3, 4, 5];
    
        rows.forEach((row) => {
            columns.forEach((column) => {
                cy.get(`:nth-child(${row}) > :nth-child(${column}) > .p-element > .p-checkbox > .p-checkbox-box`).click();
            });
        });

        // Change the role
        cy.get('#pr_id_5_label').click();
        cy.get('[ng-reflect-label="User"] > .p-ripple').click();
        cy.wait(500);
        cy.get('#pr_id_5_label').click();
        cy.get('[ng-reflect-label="SuperUserSuperUserSuperUserSup"] > .p-ripple').click();

        // Save the changes
        cy.get('.p-button-label').click();
    });
});
