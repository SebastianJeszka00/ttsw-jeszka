import SourceListPage from './SourceListPage';

describe("Measuring source list", () => {
    before(() => {
        cy.login();
        cy.SourceList();
    });

    it("should perform all operations after login", () => {
        // Add a new item
        addItem();

        // select options from dropdowns
        selectOptionsFromDropdown('#pr_id_9_label', ['Test123', '3sss233444jghjgh']);
        selectOptionsFromDropdown(':nth-child(5) > .p-inputwrapper > .p-dropdown', ['LOCAL_DRIVE', 'FTP', 'PWD', 'PWI_ENERGA', 'PWI_GODZ']);
        selectOptionsFromDropdown(':nth-child(6) > .p-inputwrapper > .p-dropdown', ['PROFILE']);
        selectOptionsFromDropdown(':nth-child(7) > .p-inputwrapper > .p-dropdown', ['txt', 'csv', 'ptpire']);

        cy.get('[ng-reflect-label="Save"]').click();
        cy.get('.p-dialog-header-close').click();

        // Import sources
        importSources();

        // Export sources
        exportSources();

        // Delete sources
        deleteSources();
    });
});

function addItem() {
    cy.get('#addSource > .p-ripple > .p-button-label').click();
    cy.get('#name').type('Tester');
    cy.get('#dateFrom > .p-calendar > .p-inputtext').type('2023-10-27');
    cy.get('#dateTo > .p-calendar > .p-inputtext').type('2023-11-13');
    cy.get(':nth-child(3) > .p-inputwrapper > .p-dropdown').click();
    cy.get('[ng-reflect-option="ENERGY"] > .p-ripple').click();
}

function selectOptionsFromDropdown(selector: string, options: string[]) {
    options.forEach((option: string) => {
        cy.get(selector).click();
        cy.get(`[ng-reflect-label="${option}"] > .p-ripple`).click();
        cy.wait(40);
    });
}

function importSources() {
    cy.get('.p-button-outlined > .p-ripple > .p-button-label').click();
    cy.get('.p-dialog-header-close-icon').click();
}

function exportSources() {
    cy.get('#exportSources > .p-ripple > .p-button-label').click();
}

function deleteSources() {
    cy.get('#deleteSources').click();
    cy.get('#deleteSourcesDialogRemove').click();
}
