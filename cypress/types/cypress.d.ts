declare namespace Cypress {
  interface Chainable<Subject> {
    /**
     * Log in to the application using credentials from the fixtures file
     */
    login(): Chainable<any>;

    /**
     * Visit the URL specified in the fixtures file
     */
    visitUrl(): Chainable<any>;

    /**
     * Navigate to Permission Group List
     */
    PermissionGroupList(): Chainable<any>;

    /**
     * Switch the application to dark mode
     */
    darkMode(): Chainable<any>;

    /**
     * Navigate to Measuring Source List
     */
    MeasuringSourceList(): Chainable<any>;

    /**
     * Change the language of the application
     */
    changeLanguage(): Chainable<any>;

    /**
     * Log out from the application
     */
    logOut(): Chainable<any>;

    /**
     * Navigate to Permission for Tab Items
     */
    PermissionForTabItems(): Chainable<any>;

    /**
     * Navigate to Source List
     */
    SourceList(): Chainable<any>;
  }
}
